<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
    @author mahfuz.ahmed
    @since 7/2/20
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head>
    <title>$Title$</title>
  </head>
  <body>
  <c:url value="/" var="home"/>
  <form:form action="${home}" method="get">
    <input name="id" type="text">
    <button type="submit">Details</button>
  </form:form>
  ${studentModel.id}
  ${studentModel.name}
  </body>
</html>
