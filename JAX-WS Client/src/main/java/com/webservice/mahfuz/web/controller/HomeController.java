package com.webservice.mahfuz.web.controller;

import com.webservice.mahfuz.service.Student;
import com.webservice.mahfuz.service.StudentServiceEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author mahfuz.ahmed
 * @since 7/2/20
 */
@Controller
public class HomeController {

    @Autowired
    private StudentServiceEndpoint studentService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String loadHome(@RequestParam(defaultValue = "0") int id, Model model) {
        if (id != 0) {
            Student student = studentService.getStudentById(id);
            model.addAttribute("studentModel", student);
        }

        return "index";
    }
}
