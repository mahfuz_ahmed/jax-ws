package com.webservice.mahfuz.dao;

import com.webservice.mahfuz.domain.Student;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author mahfuz.ahmed
 * @since 7/2/20
 */
@Repository
public class StudentDao {

    private List<Student> students;

    public StudentDao() {
        students = new ArrayList<>();
        students.add(new Student(1, "Mahfuz"));
        students.add(new Student(2, "Ahmed"));
        students.add(new Student(3, "Masum"));
    }

    public Optional<Student> findById(long id) {
        return this.students.stream()
                .filter(student -> student.getId() == id)
                .findFirst();
    }
}
