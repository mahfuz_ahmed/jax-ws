package com.webservice.mahfuz.domain;

import java.io.Serializable;

/**
 * @author mahfuz.ahmed
 * @since 7/1/20
 */
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    private long id;

    private String name;

    public Student() {

    }

    public Student(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
