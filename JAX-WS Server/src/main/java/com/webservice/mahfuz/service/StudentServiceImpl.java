package com.webservice.mahfuz.service;

import com.webservice.mahfuz.dao.StudentDao;
import com.webservice.mahfuz.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author mahfuz.ahmed
 * @since 7/1/20
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentDao studentDao;

    @Override
    public Student getStudentById(long id) {
        Optional<Student> studentOptional = studentDao.findById(id);
        return studentOptional.orElse(null);
    }
}
