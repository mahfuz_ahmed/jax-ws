package com.webservice.mahfuz.service;

import com.webservice.mahfuz.domain.Student;
import org.springframework.stereotype.Service;

/**
 * @author mahfuz.ahmed
 * @since 7/1/20
 */
@Service
public interface StudentService {

    Student getStudentById(long id);
}
