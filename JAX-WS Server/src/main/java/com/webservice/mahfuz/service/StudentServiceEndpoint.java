package com.webservice.mahfuz.service;

import com.webservice.mahfuz.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 * @author mahfuz.ahmed
 * @since 7/1/20
 */
@Component
@WebService(serviceName = "studentService")
public class StudentServiceEndpoint {

    @Autowired
    private StudentService studentService;

    @WebMethod
    public Student getStudentById(long id) {
        return studentService.getStudentById(id);
    }
}